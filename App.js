/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { StyleSheet, View, StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import store from './src/redux_store/store';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';


import SplashScreen from './src/screen/SplashScreen';
import ViewPagerScreen from './src/screen/ViewPagerScreen';
import HomeScreen from './src/screen/HomeScreen';
import PlayingVideoScreen from './src/screen/PlayingVideoScreen';

import ShortIntroScreen from './src/screen/ShortIntroScreen';
import MoreScreen from './src/screen/MoreScreen';
import ActivityScreen from './src/screen/ActivityScreen';
import SignUpScreen from './src/screen/SignUpScreen';
import GoogleSignUp from './src/components/GoogleSignUp';
import WebsiteScreen from './src/screen/WebsiteScreen';
import StoreScreen from './src/screen/StoreScreen';
import EmailLogin from './src/screen/EmailLogin';
import ProfileScreen from './src/screen/ProfileScreen';
import Colors from './src/common/Colors';



// console.disableYellowBox = true;
const Stack = createStackNavigator();
class App extends Component {
  constructor(props) {
    super(props);
    // run_database_migrations().then(() => {
    //   // console.log("creating table");
    // });
  }
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <StatusBar backgroundColor={Colors.themeColor}
            barStyle={"light-content"}
          />
          <NavigationContainer>
            <Stack.Navigator
              screenOptions={{ animationEnabled: false, headerShown: false }}
            >
              <Stack.Screen name="SplashScreen" component={SplashScreen} />
              <Stack.Screen name='HomeScreen' component={HomeScreen} />
              <Stack.Screen name='ViewPagerScreen' component={ViewPagerScreen} />
              <Stack.Screen name='PlayingVideoScreen' component={PlayingVideoScreen} />
              <Stack.Screen name="ShortIntroScreen" component={ShortIntroScreen} />
              <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
              <Stack.Screen name='MoreScreen' component={MoreScreen} />
              <Stack.Screen name='StoreScreen' component={StoreScreen} />
              <Stack.Screen name='ActivityScreen' component={ActivityScreen} />
              <Stack.Screen name='SignUpScreen' component={SignUpScreen} />
              <Stack.Screen name='GoogleSignUp' component={GoogleSignUp} />
              <Stack.Screen name='WebsiteScreen' component={WebsiteScreen} />
              <Stack.Screen name='EmailLogin' component={EmailLogin} />
              
            </Stack.Navigator>
          </NavigationContainer>
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

export default App;
