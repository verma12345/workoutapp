import store from '../store';
import { SET_LOADING } from '../types/types';
import { IS_FULL_SCREEN, IS_HEALTH, IS_LOADING_VIDEO, IS_PAUSED, IS_RESOURCES, SEE_ALL1, SET_ACTIVE_DOT, SET_AGE, SET_CURRENT_TIME, SET_DURATION, SET_FULL_NAME, SET_HEIGHT, SET_IS_END, SET_NAME, SET_PLAYER_STATE, SET_SCREEN_TYPE, SET_VIDEO_PLAYER, SET_VIDEO_URL, SET_VOLUME, SET_WEIGHT } from '../types/typesJaswant';




export const setActiveDot = (index) => (dispatch) => {
  // console.log(index);
  dispatch({
    type: SET_ACTIVE_DOT,
    payload: index,
  });
};

// video player 
export const setVideoPlayer = (player) => (dispatch) => {
  dispatch({
    type: SET_VIDEO_PLAYER,
    payload: player,
  });
};

export const setCurrentTime = (currentTime) => (dispatch) => {
  dispatch({
    type: SET_CURRENT_TIME,
    payload: currentTime,
  });
};

export const setDuration = (duration) => (dispatch) => {
  dispatch({
    type: SET_DURATION,
    payload: duration,
  });
};

export const setIsFullScreen = (isFullScreen) => (dispatch) => {
  dispatch({
    type: IS_FULL_SCREEN,
    payload: isFullScreen,
  });
};

export const isLoadingVideo = (isLoading) => (dispatch) => {
  dispatch({
    type: IS_LOADING_VIDEO,
    payload: isLoading,
  });
};

export const setPaused = (paused) => (dispatch) => {
  dispatch({
    type: IS_PAUSED,
    payload: paused,
  });
};

export const setScreenType = (screenType) => (dispatch) => {
  dispatch({
    type: SET_SCREEN_TYPE,
    payload: screenType,
  });
};

export const setPlayerState = (playerState) => (dispatch) => {
  dispatch({
    type: SET_PLAYER_STATE,
    payload: playerState,
  });
};


export const setVolume = (volume) => (dispatch) => {
  // console.log(volume);
  dispatch({
    type: SET_VOLUME,
    payload: volume,
  });
};


export const setVideoUrl = (url) => (dispatch) => {
  dispatch({
    type: SET_VIDEO_URL,
    payload: url,
  });
};

export const setSeeAll = (isTrue) => (dispatch) => {
  dispatch({
    type: SEE_ALL1,
    payload: isTrue,
  });
};


export const setIsEndVideo = (isTrue) => (dispatch) => {
  dispatch({
    type: SET_IS_END,
    payload: isTrue,
  });
};

export const hitVimeoAPI = (param) => {
  return async (dispatch) => {
    dispatch({
      type: SET_LOADING,
      payload: true,
    });

    let data = await fetch(``, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        "Access-Control-Allow-Origin": "*",
        'Content-Type': 'multipart/form-data',
      },
      body: param,
    });
    // console.log(data);
    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};


// Profile Screen

export const setName = (name) => (dispatch) => {
  dispatch({
    type: SET_NAME,
    payload: name,
  });
};

export const setFullName = (full_name) => (dispatch) => {
  dispatch({
    type: SET_FULL_NAME,
    payload: full_name,
  });
};

export const setAge = (age) => (dispatch) => {
  dispatch({
    type: SET_AGE,
    payload: age,
  });
};

export const setWeight = (weight) => (dispatch) => {
  dispatch({
    type: SET_WEIGHT,
    payload: weight,
  });
};

export const setHeight = (height) => (dispatch) => {
  dispatch({
    type: SET_HEIGHT,
    payload: height,
  });
};