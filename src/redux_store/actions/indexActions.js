import store from '../store';
import {
  PROGRESS_BAR,
} from './types';


export const setProgress = (num) => (dispatch) => {
  let state = store.getState().indexReducer;
  let count = state.progress + 1;
  if (num==1) {
    // debugLog("stop")
    dispatch({
      type: PROGRESS_BAR,
      payload: 0,
    });
  }
  dispatch({
    type: PROGRESS_BAR,
    payload: count,
  });
};


