import { SET_IS_LOADING, IS_PROFILE_VISIBLE, IS_VISIBLE_PASSWORD, SET_EMAIL, SET_PASSWORD, SET_PROFILE_PIC, SET_INDEX, IS_STORE, IS_PRIVACY_POLICY, IS_TERMS_AND_CONDITION } from "../types/typesAkshita";


export const setProgress = (num) => (dispatch) => {
  let state = store.getState().indexReducer;
  let count = state.progress + 1;
  if (num==1) {
    // debugLog("stop")
    dispatch({
      type: PROGRESS_BAR,
      payload: 0,
    });
  }
  dispatch({
    type: PROGRESS_BAR,
    payload: count,
  });
};


export const setProfilePic = (pic) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_PIC,
    payload: pic,
  });

  dispatch({
    type: IS_PROFILE_VISIBLE,
    payload: true,
  });
};

export const setEmail = (email) => (dispatch) => {

  dispatch({
    type: SET_EMAIL,
    payload: email,
  });
};

export const setPassword = (password) => (dispatch) => {

  dispatch({
    type: SET_PASSWORD,
    payload: password,
  });
};

export const setPasswordVisibility = (is_visible_pass) => (dispatch) => {

  dispatch({
    type: IS_VISIBLE_PASSWORD,
    payload: is_visible_pass,
  });
};

export const setIsLoading = (isLoading) => (dispatch) => {

  dispatch({
    type: SET_IS_LOADING,
    payload: isLoading,
  });
};

export const setIndex = (index) => (dispatch) => {
// console.log(index);
  dispatch({
    type: SET_INDEX,
    payload: index,
  });
};

// more screen
export const setIsStore = (isStore) => (dispatch) => {

  dispatch({
    type: IS_STORE,
    payload:isStore,
  });
};
export const setIsPrivacyPolicy = (isPrivacyPolicy) => (dispatch) => {

  dispatch({
    type: IS_PRIVACY_POLICY,
    payload: isPrivacyPolicy,
  });
};
export const setIsTermsAndCondition = (isTermsAndCondition) => (dispatch) => {

  dispatch({
    type: IS_TERMS_AND_CONDITION,
    payload: isTermsAndCondition,
  });
};
