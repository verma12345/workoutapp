
import { PLAYER_STATES } from 'react-native-media-controls';
import {
  IS_FULL_SCREEN, IS_LOADING_VIDEO, IS_PAUSED, SEE_ALL1, SET_ACTIVE_DOT,
  SET_AGE,
  SET_CURRENT_TIME, SET_DURATION, SET_FULL_NAME, SET_HEIGHT, SET_IS_END, SET_NAME, SET_VIDEO_PLAYER, SET_VIDEO_URL,
  SET_VOLUME,
  SET_WEIGHT
} from '../types/typesJaswant';

const initialState = {

  position: 0,
  dot_list: [
    {
      index: 0,
    },
    {
      index: 1,
    },
    {
      index: 2,
    },
    {
      index: 3,
    },
  ],
  // Video player
  trending_list: [
    {
      description: "Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge.\n\nLicensed under the Creative Commons Attribution license\nhttp://www.bigbuckbunny.org",
      sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
      subtitle: "By Blender Foundation",
      thumb: "images/BigBuckBunny.jpg",
      title: "Big Buck Bunny",
    },
  ],

  video_list: [
    {
      description: "Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge.\n\nLicensed under the Creative Commons Attribution license\nhttp://www.bigbuckbunny.org",
      sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
      subtitle: "By Blender Foundation",
      thumb: require("../../../assets/thumb/BigBuckBunny.png"),
      title: "Big Buck Bunny",
    },
    {
      description: "The first Blender Open Movie from 2006",
      sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4",
      subtitle: "By Blender Foundation",
      thumb: require("../../../assets/thumb/ElephantsDream.jpeg"),
      title: "Elephant Dream"
    },
    {
      description: "HBO GO now works with Chromecast -- the easiest way to enjoy online video on your TV. For when you want to settle into your Iron Throne to watch the latest episodes. For $35.\nLearn how to use Chromecast with HBO GO and more at google.com/chromecast.",
      sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
      subtitle: "By Google",
      thumb: require("../../../assets/thumb/ForBiggerBlazes.jpg"),
      title: "For Bigger Blazes"
    },
    {
      description: "Introducing Chromecast. The easiest way to enjoy online video and music on your TV—for when Batman's escapes aren't quite big enough. For $35. Learn how to use Chromecast with Google Play Movies and more at google.com/chromecast.",
      sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
      subtitle: "By Google",
      thumb: require("../../../assets/thumb/ForBiggerEscapes.jpg"),
      title: "For Bigger Escape"
    },
    {
      description: "Introducing Chromecast. The easiest way to enjoy online video and music on your TV. For $35.  Find out more at google.com/chromecast.",
      sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4",
      subtitle: "By Google",
      thumb: require("../../../assets/thumb/ForBiggerFun.jpg"),
      title: "For Bigger Fun"
    },
    // {
    //   description: "Introducing Chromecast. The easiest way to enjoy online video and music on your TV—for the times that call for bigger joyrides. For $35. Learn how to use Chromecast with YouTube and more at google.com/chromecast.",
    //   sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
    //   subtitle: "By Google",
    //   thumb: require("../../../assets/thumb/ForBiggerJoyrides.jpg"),
    //   title: "For Bigger Joyrides"
    // },
  ],
  video_url: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',

  videoPlayer: null,
  currentTime: 0,
  duration: 0,
  isFullScreen: false,
  isLoading: false,
  paused: false,
  playerState: PLAYER_STATES.PLAYING,
  screenType: 'contain',
  volume_is: 0,

  seeAll1: false,
  is_end: false,

  // PROFILE SCREEN
  name: 'Name',
  full_name: "Full Name",
  age: "22",
  weight: "14 Stone 4 Pound",
  height: `6' 4"`,

};

export default function (state = initialState, action) {
  switch (action.type) {

    case SET_ACTIVE_DOT:
      return {
        ...state,
        position: action.payload,
      };


    // video player
    case SET_VIDEO_PLAYER:
      return {
        ...state,
        videoPlayer: action.payload,
      };

    case SET_DURATION:
      return {
        ...state,
        duration: action.payload,
      };

    case SET_CURRENT_TIME:
      return {
        ...state,
        currentTime: action.payload,
      };

    case IS_FULL_SCREEN:
      return {
        ...state,
        isFullScreen: action.payload,
      };

    case IS_LOADING_VIDEO:
      return {
        ...state,
        isLoading: action.payload,
      };

    case IS_PAUSED:
      return {
        ...state,
        paused: action.payload,
      };

    case SET_VOLUME:
      return {
        ...state,
        volume_is: action.payload,
      };

    case SET_VIDEO_URL:
      return {
        ...state,
        video_url: action.payload,
      };

    case SEE_ALL1:
      return {
        ...state,
        seeAll1: action.payload,
      };

    case SET_IS_END:
      return {
        ...state,
        is_end: action.payload,
      };


    // Profile Screen
    case SET_NAME:
      return {
        ...state,
        name: action.payload,
      };

    case SET_FULL_NAME:
      return {
        ...state,
        full_name: action.payload,
      };

    case SET_AGE:
      return {
        ...state,
        age: action.payload,
      };

    case SET_WEIGHT:
      return {
        ...state,
        weight: action.payload,
      };

    case SET_HEIGHT:
      return {
        ...state,
        height: action.payload,
      };


    default:
      return state;
  }
}