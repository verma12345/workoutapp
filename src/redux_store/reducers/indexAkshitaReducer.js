import { SET_IS_LOADING, IS_PROFILE_VISIBLE, IS_VISIBLE_PASSWORD, SET_EMAIL, SET_PASSWORD, SET_PROFILE_PIC, SET_INDEX, IS_STORE, IS_PRIVACY_POLICY, IS_TERMS_AND_CONDITION } from '../types/typesAkshita';

const initialState = {

  achievements_list: [
    {
      image: require('../../../assets/image/workout.png'),
      title: "First Workout",
    },
    {
      image: require('../../../assets/image/punche.png'),
      title: "5k Punches",
    },
    {
      image: require('../../../assets/image/first.png'),
      title: "Prospect Path",
    },
    {
      image: require('../../../assets/image/day3.png'),
      title: "3-Day Streak",
    },
    {
      image: require('../../../assets/image/score.png'),
      title: "Beat Your Score ",
    },
    {
      image: require('../../../assets/image/workout.png'),
      title: "First Workout",
    },
    {
      image: require('../../../assets/image/punche.png'),
      title: "5k Punches",
    },
    {
      image: require('../../../assets/image/first.png'),
      title: "Prospect Path",
    },
    {
      image: require('../../../assets/image/day3.png'),
      title: "3-Day Streak",
    },
    {
      image: require('../../../assets/image/score.png'),
      title: "Beat Your Score ",
    },
  ],


  video_list: [

    {
      title: "COMPLETED ACTIVITIES",
      data: [{
        description: "Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge.\n\nLicensed under the Creative Commons Attribution license\nhttp://www.bigbuckbunny.org",
        sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
        subtitle: "By Blender Foundation",
        thumb: require("../../../assets/thumb/BigBuckBunny.png"),
      },
      {
        description: "The first Blender Open Movie from 2006",
        sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4",
        subtitle: "By Blender Foundation",
        thumb: require("../../../assets/thumb/ElephantsDream.jpeg"),
        title: "Elephant Dream"
      },
      {
        description: "HBO GO now works with Chromecast -- the easiest way to enjoy online video on your TV. For when you want to settle into your Iron Throne to watch the latest episodes. For $35.\nLearn how to use Chromecast with HBO GO and more at google.com/chromecast.",
        sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
        subtitle: "By Google",
        thumb: require("../../../assets/thumb/ForBiggerBlazes.jpg"),
        title: "For Bigger Blazes"
      },
      {
        description: "Introducing Chromecast. The easiest way to enjoy online video and music on your TV—for when Batman's escapes aren't quite big enough. For $35. Learn how to use Chromecast with Google Play Movies and more at google.com/chromecast.",
        sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
        subtitle: "By Google",
        thumb: require("../../../assets/thumb/ForBiggerEscapes.jpg"),
        title: "For Bigger Escape"
      },
      {
        description: "Introducing Chromecast. The easiest way to enjoy online video and music on your TV. For $35.  Find out more at google.com/chromecast.",
        sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4",
        subtitle: "By Google",
        thumb: require("../../../assets/thumb/ForBiggerFun.jpg"),
        title: "For Bigger Fun"
      },
      {
        description: "Introducing Chromecast. The easiest way to enjoy online video and music on your TV—for the times that call for bigger joyrides. For $35. Learn how to use Chromecast with YouTube and more at google.com/chromecast.",
        sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
        subtitle: "By Google",
        thumb: require("../../../assets/thumb/ForBiggerJoyrides.jpg"),
        title: "For Bigger Joyrides"
      },
      ]
    },


    {
      title: "NEXT ACTIVITIES",
      data: [{
        description: "Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge.\n\nLicensed under the Creative Commons Attribution license\nhttp://www.bigbuckbunny.org",
        sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
        subtitle: "By Blender Foundation",
        thumb: require("../../../assets/thumb/BigBuckBunny.png"),
      },
      {
        description: "The first Blender Open Movie from 2006",
        sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4",
        subtitle: "By Blender Foundation",
        thumb: require("../../../assets/thumb/ElephantsDream.jpeg"),
        title: "Elephant Dream"
      },
      {
        description: "HBO GO now works with Chromecast -- the easiest way to enjoy online video on your TV. For when you want to settle into your Iron Throne to watch the latest episodes. For $35.\nLearn how to use Chromecast with HBO GO and more at google.com/chromecast.",
        sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
        subtitle: "By Google",
        thumb: require("../../../assets/thumb/ForBiggerBlazes.jpg"),
        title: "For Bigger Blazes"
      },
      {
        description: "Introducing Chromecast. The easiest way to enjoy online video and music on your TV—for when Batman's escapes aren't quite big enough. For $35. Learn how to use Chromecast with Google Play Movies and more at google.com/chromecast.",
        sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
        subtitle: "By Google",
        thumb: require("../../../assets/thumb/ForBiggerEscapes.jpg"),
        title: "For Bigger Escape"
      },
      {
        description: "Introducing Chromecast. The easiest way to enjoy online video and music on your TV. For $35.  Find out more at google.com/chromecast.",
        sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4",
        subtitle: "By Google",
        thumb: require("../../../assets/thumb/ForBiggerFun.jpg"),
        title: "For Bigger Fun"
      },
      {
        description: "Introducing Chromecast. The easiest way to enjoy online video and music on your TV—for the times that call for bigger joyrides. For $35. Learn how to use Chromecast with YouTube and more at google.com/chromecast.",
        sources: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
        subtitle: "By Google",
        thumb: require("../../../assets/thumb/ForBiggerJoyrides.jpg"),
        title: "For Bigger Joyrides"
      },
      ]
    },
  ],

  // Update profile
  set_profile: '',
  is_profile_visible: false,

  set_email: '',
  set_password: '',
  is_visible_pass: false,

  is_loading: false,
  index: '',

  // More screen
  isStore: 'false',
  isPrivacyPolicy: 'false',
  isTermsAndCondition: 'false'
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_PROFILE_PIC:
      return {
        ...state,
        set_profile: action.payload,
      };

    case IS_PROFILE_VISIBLE:
      return {
        ...state,
        is_profile_visible: action.payload,
      };

    case SET_EMAIL:
      return {
        ...state,
        set_email: action.payload,
      };

    case SET_PASSWORD:
      return {
        ...state,
        set_password: action.payload,
      };

    case IS_VISIBLE_PASSWORD:
      return {
        ...state,
        is_visible_pass: action.payload,
      };

    case SET_IS_LOADING:
      return {
        ...state,
        is_loading: action.payload,
      };

    case SET_INDEX:
      return {
        ...state,
        index: action.payload,
      };

    // more screen


    case IS_STORE:
      return {
        ...state,
        isStore: action.payload,
      };

    case IS_PRIVACY_POLICY:
      return {
        ...state,
        isPrivacyPolicy: action.payload,
      };

    case IS_TERMS_AND_CONDITION:
      return {
        ...state,
        isTermsAndCondition: action.payload,
      };
    default:
      return state;
  }
}
