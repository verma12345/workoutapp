import { combineReducers } from 'redux';
import indexAkshitaReducer from './indexAkshitaReducer';
import indexJasReducer from './indexJasReducer';
import indexReducer from './indexReducer';


export default combineReducers({
    indexReducer: indexReducer,
    indexJasReducer: indexJasReducer,
    indexAkshitaReducer:indexAkshitaReducer

})

