
export const MODULE_KEY = 'module_app_store_';
// change width
export const SET_ACTIVE_DOT = MODULE_KEY + 'SET_ACTIVE_DOT';

// vedo player

export const SET_CURRENT_TIME = MODULE_KEY + 'SET_CURRENT_TIME';
export const SET_DURATION = MODULE_KEY + 'SET_DURATION';
export const IS_FULL_SCREEN = MODULE_KEY + 'IS_FULL_SCREEN';
export const IS_LOADING_VIDEO = MODULE_KEY + 'IS_LOADING_VIDEO';
export const IS_PAUSED = MODULE_KEY + 'IS_PAUSED';
export const SET_SCREEN_TYPE = MODULE_KEY + 'SET_SCREEN_TYPE';
export const SET_PLAYER_STATE = MODULE_KEY + 'SET_PLAYER_STATE';
export const SET_VIDEO_PLAYER = MODULE_KEY + 'SET_VIDEO_PLAYER';
export const SET_VIDEO_URL = MODULE_KEY + 'SET_VIDEO_URL';


export const SET_VOLUME = MODULE_KEY + 'SET_VOLUME';
export const SET_MUTE = MODULE_KEY + 'SET_MUTE';

export const SEE_ALL1 = MODULE_KEY + 'SEE_ALL1';
export const SET_IS_END = MODULE_KEY + 'SET_IS_END';

// Profile Screen
export const SET_NAME = MODULE_KEY + 'SET_NAME';
export const SET_FULL_NAME = MODULE_KEY + 'SET_FULL_NAME';
export const SET_AGE = MODULE_KEY + 'SET_AGE';
export const SET_WEIGHT = MODULE_KEY + 'SET_WEIGHT';
export const SET_HEIGHT = MODULE_KEY + 'SET_HEIGHT';




