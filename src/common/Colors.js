const tintColor = '#66ffff';
const whiteText = '#ffffff';
const statusBarColor = '#1050E6';
const backgroundColor = '#1050E6';
const placeHolderColor = '#C4C4C4';
const textColor = '#343434';
const textDarkColor = "#3D3D3D"
const themeColor = '#061721';
const grayColor = '#737373';
const headerColor = '#c5c8cd';
const lightgray = '#bdbdbd'



export default {
  tintColor,
  tabIconDefault: '#384659',
  tabIconSelected: tintColor,
  whiteText,
  themeColor,
  statusBarColor,
  red: "red",
  grayColor,
  backgroundColor,
  placeHolderColor,
  textColor,
  textDarkColor,
  headerColor,
  lightgray
};
