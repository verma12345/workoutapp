import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { getWidth } from '../common/Layout';
import Font from '../common/Font';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { hitVimeoAPI } from '../redux_store/actions/indexActionsJaswant';
import Prefs, { getPrefs } from '../common/Prefs';
import Constants from '../common/Constants';
import { StackActions } from '@react-navigation/native';

// import Vimeo from 'vimeo'
// let Vimeo = require('vimeo').Vimeo;
// let client = new Vimeo("{client_id}", "{client_secret}", "{access_token}");

class SplashScreen extends Component {
  constructor(props) {
    super(props);
  }


  componentDidMount() {
    let time = Constants.DEBUG ? 1000 : 3000;
    setTimeout(() => {
      getPrefs(Prefs.IS_REGISTER).then((isRegister) => {
        if (isRegister != '' && isRegister != undefined) {
          // this.props.navigation.navigate('ShortIntroScreen');
          this.props.navigation.dispatch(
            StackActions.replace('ShortIntroScreen'),
          )
        } else {
          this.props.navigation.dispatch(
            StackActions.replace('SignUpScreen'),
          )
        }
      });
    }, time);
  }



  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.mainViewStyle}>
          <Image
            source={require('../../assets/image/splash_screen.png')}
            style={styles.imgStyle}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  imgStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    resizeMode: 'cover'
  },

  mainViewStyle: {
    flex: 1,
  },
});
const mapStateToProps = (state) => {
  const ss = state.indexReducer;

  return {
    // vendor_id: ss.vendor_id,

  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // setVenderId: (user_id) => setVenderId(user_id),
      hitVimeoAPI: (param) => hitVimeoAPI(param),

    },
    dispatch,
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
