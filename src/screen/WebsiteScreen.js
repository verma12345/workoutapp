import React, { Component } from 'react';
import { ActivityIndicator, Image, StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import WebView from 'react-native-webview';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import Header from '../components/Header';
import { setIsLoading } from '../redux_store/actions/indexActionsAkshita';

class WebsiteScreen extends Component {
    constructor(props) {
        super(props);
    }

    renderLoadingView() {
        return (
            <View style={{
                borderRadius: 10,
                backgroundColor: "white",
                flex: 1,

            }}>

                <ActivityIndicator
                    size={"large"}
                    color="#bdcf24"
                    hidesWhenStopped={true}
                />

            </View>
        );
    }
    render() {
        return (
            <View style={{ flex: 1 }} >

             
                {/* <Header
                        bgColor='white'
                        barStyle="light-content"
                        isBack={true}
                        navigation={this.props.navigation}
                    />  */}
                <View style={{ height: 45, backgroundColor: Colors.whiteText }}>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                        <Image
                          style={{height:30,width:35,marginLeft:15,tintColor:"black"}}
                          source={require('../../assets/image/back.png')}
                        />
                    </TouchableOpacity>
                </View>

                <WebView
                    source={{ uri: this.props.route.params.url }}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    originWhitelist={['*']}
                    renderLoading={this.renderLoadingView} startInLoadingState={true}
                />


            </View>
        )
    }
}

const mapStateToProps = (state) => {
    const ss = state.indexReducer;
    const stateAkshita = state.indexAkshitaReducer;

    return {
        // vendor_id: ss.vendor_id,
        is_loading: stateAkshita.is_loading
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            // setVenderId: (user_id) => setVenderId(user_id),
            setIsLoading: (is_loading) => setIsLoading(is_loading)

        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(WebsiteScreen);

