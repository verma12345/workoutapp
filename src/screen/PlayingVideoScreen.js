import React, { Component } from 'react'
import {
    StyleSheet, View, Text, TouchableOpacity,
    Dimensions, StatusBar, Image
} from 'react-native'
import Orientation from 'react-native-orientation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isLoadingVideo, setCurrentTime, setDuration, setIsEndVideo, setIsFullScreen, setPaused, setPlayerState, setScreenType, setVideoPlayer } from '../redux_store/actions/indexActionsJaswant';
import SelectedVideoPlayer from '../components/SelectedVideoPlayer';
import EndVideo from '../components/EndVideo';

class PlayingVideoScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLandscape: false

        }
    }

    componentWillUnmount() {
        this.props.setIsEndVideo(false)
        Orientation.lockToPortrait()

    }

    changeOreantation = () => {
        if (this.state.isLandscape) {
            Orientation.lockToLandscape()
            this.setState({ isLandscape: false })
        } else {
            Orientation.lockToPortrait()
            this.setState({ isLandscape: true })

        }
    }


    _setVolume = () => {
        if (this.props.volume_is == 0) {
            this.props.setVolume(30);
        } else {
            this.props.setVolume(0)
        }
    }
    _onClick = () => {
        alert('Comming Soon...')
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar hidden />


                <SelectedVideoPlayer
                    video_url={this.props.video_url}
                    volume_is={this.props.volume_is}
                    setIsEndVideo={this.props.setIsEndVideo}
                    is_end={this.props.is_end}
                />

                {this.props.is_end ? <View style={{
                    position: 'absolute',
                    top: 0,
                    alignSelf: "flex-end",
                    flexDirection: 'row',
                    justifyContent: "space-around",
                    borderBottomLeftRadius: 10
                }} >
                    <EndVideo />

                </View> : null}


                <View style={{
                    position: 'absolute',
                    bottom: 0,
                    alignSelf: "flex-end",
                    flexDirection: 'row',
                    justifyContent: "space-around",
                    alignItems: "center"
                }} >


                    <TouchableOpacity
                        onPress={() => {
                            this.changeOreantation()
                        }}
                        style={{ margin: 10 }}

                    >
                        <Image
                            style={{ width: 15, height: 15 }}
                            source={require("../../assets/image/full.png")}
                        />

                    </TouchableOpacity>

                    {this.props.is_end ?
                        <TouchableOpacity
                            onPress={() => { this._onClick() }}
                        >
                            <Image
                                source={require("../../assets/image/play1.png")}
                                style={{ height: 40, width: 40, margin: 20, resizeMode: 'contain' }}
                            />
                        </TouchableOpacity> :
                        null
                    }
                </View>


            </View>
        )
    }
}

const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0000',

    },
    backgroundVideo: {
        backgroundColor: "black"
    },
    toolbar: {
        marginTop: 30,
        padding: 10,
        borderRadius: 5,
    },

    toolbar: {
        backgroundColor: "white",
        padding: 10,
        borderRadius: 5
    }
})


const mapStateToProps = (state) => {
    const s = state.indexReducer;
    const stateAkshita = state.indexAkshitaReducer;
    const Jaswant = state.indexJasReducer;


    return {

        is_ready: stateAkshita.is_ready,

        // video player
        videoPlayer: Jaswant.videoPlayer,
        currentTime: Jaswant.currentTime,
        duration: Jaswant.duration,
        isFullScreen: Jaswant.isFullScreen,
        isLoading: Jaswant.isLoading,
        paused: Jaswant.paused,
        playerState: Jaswant.playerState,
        screenType: Jaswant.screenType,

        volume_is: Jaswant.volume_is,
        video_url: Jaswant.video_url,
        is_end: Jaswant.is_end,
        // *************************
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setIsReady: (is_ready) => setIsReady(is_ready),

            // video player
            setVideoPlayer: (videoPlayer) => setVideoPlayer(videoPlayer),
            setCurrentTime: (currentTime) => setCurrentTime(currentTime),
            setDuration: (duration) => setDuration(duration),
            setIsFullScreen: (isFullScreen) => setIsFullScreen(isFullScreen),
            isLoadingVideo: (isLoading) => isLoadingVideo(isLoading),
            setPaused: (paused) => setPaused(paused),
            setScreenType: (screenType) => setScreenType(screenType),
            setPlayerState: (playerState) => setPlayerState(playerState),
            setVolume: (volume) => setVolume(volume),
            // ************************************
            setIsEndVideo: (isTrue) => setIsEndVideo(isTrue),


        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(PlayingVideoScreen);