import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    Image,
    TouchableOpacity,
    Dimensions,
    FlatList,
    ToastAndroid,
    SectionList,
    TextInput,
    ScrollView
} from 'react-native';
import { getWidth } from '../common/Layout';
import Font from '../common/Font';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import BottomNav from '../components/BottomNav';
import Header from '../components/Header';
import ViewMoreText from 'react-native-view-more-text';


class ActivityScreen extends Component {
    constructor(props) {
        super(props);
    }

    renderViewMore(onPress) {
        return (
            <Text style={{ color: 'blue', width: 150, height: 70 }} onPress={onPress}>{"Read more..."}</Text>
        )
    }
    renderViewLess(onPress) {
        return (
            <Text style={{ color: 'blue', width: 150, height: 70 }} onPress={onPress}>{'Less...'}</Text>
        )
    }

    _renderItem = (item) => {
        return (
            <TouchableOpacity
                onPress={() => alert('Comming Soon...')}
                style={{
                    flexDirection: "row",
                    flex: 1,
                    height: 120,
                    margin: 5,
                    elevation: 15,
                    backgroundColor: "white",
                    paddingVertical: 10,
                    borderRadius: 10
                }} >
                <Image
                    source={item.item.thumb}
                    style={{ width: 150, height: 110, borderRadius: 5, }}
                />
                        <Text style={{height:100}}>
                            {item.item.description}
                        </Text>

            </TouchableOpacity>
        )
    }

    _renderSectionHeader = (dataItem) => {
        return (
            <View
                style={{
                    height: 50, paddingHorizontal: 10,
                    margin:5,
                    borderTopLeftRadius:10,
                    borderBottomRightRadius:10,
                    justifyContent: "center", elevation: 19, backgroundColor: "#bdcf24"
                }}
            >
                <Text style={styles.sectionHeader}>{dataItem.title}</Text>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.mainViewStyle}>
                    <Header
                        bgColor={Colors.whiteText}
                        barStyle="dark-content"
                        title="Activity"

                    />


                    <SectionList
                        sections={this.props.video_list}
                        renderItem={this._renderItem}
                        renderSectionHeader={({ section }) => this._renderSectionHeader(section)}
                        keyExtractor={(item, index) => index}
                    />


                    <BottomNav
                        navigation={this.props.navigation}
                        bgColor={'#bdcf24'}
                        isActivity={true}
                    />
                </View>
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        width, height,
        flex: 1,
        // justifyContent: 'center',
        backgroundColor: 'white',
    },
    
    mainViewStyle: {
        flex: 1,
        // backgroundColor: Colors.themeColor
        backgroundColor: 'white'
    },
    sectionHeader: {
        color: Colors.textColor,
        fontSize: 18,
        fontFamily: Font.SourceSansPro,
    }
});
const mapStateToProps = (state) => {
    const ss = state.indexReducer;
    const stateAkshita = state.indexAkshitaReducer;

    return {
        // vendor_id: ss.vendor_id,
        video_list: stateAkshita.video_list
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            // setVenderId: (user_id) => setVenderId(user_id),

        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(ActivityScreen);
