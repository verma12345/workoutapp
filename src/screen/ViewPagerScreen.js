import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setActiveDot } from '../redux_store/actions/indexActionsJaswant';

class ViewPagerScreen extends Component {
constructor(props){
    super(props);
}

    render() {
        let active_dot = this.props.list.map((item, index) => {
            return (
                <View
                    key={index}
                    style={{
                        height: 10,
                        width: 10,
                        backgroundColor: 'white',
                        justifyContent: "center",
                        alignItems: 'center',
                        borderRadius: 5,
                        marginBottom: 5,
                        marginHorizontal: 3,
                        elevation: 5,
                    }} >

                    <View style={{
                        height: 7,
                        width: 7,
                        backgroundColor: this.props.position == index ? '#95a9ad': 'white',
                        borderRadius: 10,
                        elevation: 5
                    }} />
                </View>
            );
        });


        return (
            <View style={{ height:10,margin:20, }}>
                <View style={{
                    position: 'absolute',
                    flexDirection: 'row',
                    // bottom: 0,
                    alignSelf: "center",
                }}>
                    {active_dot}
                </View>

            </View>
        )
    }
}


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const jas = state.indexJasReducer;

    return {
        dot_list: jas.dot_list,
        position: jas.position,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setActiveDot: (position) => setActiveDot(position),
        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(ViewPagerScreen);
