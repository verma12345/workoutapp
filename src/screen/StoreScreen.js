import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    Image,
    TouchableOpacity,
    Dimensions,
    FlatList,
    ToastAndroid,
    ActivityIndicator,
} from 'react-native';
import { getWidth } from '../common/Layout';
import Font from '../common/Font';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import BottomNav from '../components/BottomNav';
import Header from '../components/Header';
import ProfileSection from '../components/ProfileSection';
import AchivementOption from '../components/AchivementOption';
import WebView from 'react-native-webview';



class StoreScreen extends Component {
    constructor(props) {
        super(props);
    }

    _onEdit = () => {
        ToastAndroid.show('Comming soon..', ToastAndroid.SHORT)
    }

    renderItem = (dataItem) => {
        return (

            <AchivementOption
                data_item={dataItem.item}
            />
        )
    }
    renderLoadingView() {
        return (
            <View style={{
                borderRadius: 10,
                backgroundColor: "white",
                flex: 1,

            }}>
                <ActivityIndicator
                    size={"large"}
                    color="#bdcf24"
                    hidesWhenStopped={true}
                />

            </View>
        );
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.mainViewStyle}>
                    <Header
                        bgColor={Colors.whiteText}
                        barStyle="dark-content"
                        isCross={true}
                        title="Store"
                        navigation={this.props.navigation}
                    />

                    <WebView
                        source={{ uri: this.props.route.params.url }}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        originWhitelist={['*']}
                        renderLoading={this.renderLoadingView} startInLoadingState={true}
                    />

                    <BottomNav
                        navigation={this.props.navigation}
                        bgColor={'#bdcf24'}
                        isStore={true}
                    />

                </View>
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        width, height,
        flex: 1,
        // justifyContent: 'center',
        backgroundColor: 'white',
    },
    imgStyle: {
        width: getWidth(130),
        height: getWidth(130),
        alignSelf: 'center'
    },
    textStyle2: {
        fontSize: 18,
        fontWeight: '900',
        fontFamily: Font.Roboto,
        color: Colors.textDarkColor,
    },
    textStyle1: {
        fontSize: 14,
        fontWeight: '400',
        fontFamily: Font.Roboto,
        color: Colors.grayColor,
        textDecorationLine: 'underline'
    },
    mainViewStyle: {
        flex: 1
    },
});
const mapStateToProps = (state) => {
    const ss = state.indexReducer;
    const stateAkshita = state.indexAkshitaReducer;

    return {
        // vendor_id: ss.vendor_id,
        achievements_list: stateAkshita.achievements_list
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            // setVenderId: (user_id) => setVenderId(user_id),

        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(StoreScreen);
