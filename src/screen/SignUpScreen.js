import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MyButton from '../components/MyButton';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';
import Prefs, { setPrefs } from '../common/Prefs';
import { setProfilePic } from '../redux_store/actions/indexActionsAkshita';
import { setAge, setFullName, setHeight, setName, setWeight } from '../redux_store/actions/indexActionsJaswant';

class SignUpScreen extends Component {
  constructor(props) {
    super(props);
  }
  _onLoginApple = () => {
    this.props.navigation.navigate('ShortIntroScreen')
  }
  _onEmailSignup = () => {
    this.props.navigation.navigate('EmailLogin')
  }

  _onSignup = () => {
    this.props.navigation.navigate('GoogleSignUp')
  }

  componentDidMount() {
    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
      webClientId: '698858406842-7s4qfg5crok9dp2p5ircpqv0no2v9et9.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      // hostedDomain: '', // specifies a hosted domain restriction
      // loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
      forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
      // accountName: '', // [Android] specifies an account name on the device that should be used
      // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    });
  }

  _googleSignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      //   this.setState({ userInfo });
      console.log({ userInfo });
      setPrefs(Prefs.IS_REGISTER, "1");
      this.props.setProfilePic(userInfo.user.photo)
      this.props.setName(userInfo.user.givenName)
      this.props.setFullName(userInfo.user.name)

      this.props.navigation.navigate('ShortIntroScreen')
      // alert(
      //     +"\n" + userInfo.user.givenName
      //     + "\n" + userInfo.user.familyName 
      //     + "\n" + userInfo.user.email
      //     +"\n" + userInfo.user.name
      //     +"\n" + userInfo.user.photo
      //     )
    } catch (error) {
      console.log({ error });
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      ToastAndroid.show("Signed out", ToastAndroid.SHORT, ToastAndroid.CENTER)
      console.log('signed out');
      //   this.setState({ user: null }); // Remember to remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };


  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ImageBackground
          source={require('../../assets/image/splash_screen.png')}
          imageStyle={{
            flex: 1,
            justifyContent: 'flex-end'
          }}

          style={styles.mainViewStyle}>

          <MyButton
            icon={require('../../assets/image/apple.png')}
            title='Login with Apple'
            backgroundColor="#3f3f3f"
            onPress={() => this._onLoginApple()}
          />

          <MyButton
            icon={require('../../assets/image/google.png')}
            title='Login with Google'
            backgroundColor="#d86647"
            onPress={() => this._googleSignIn()}
          // onPress={() => this._onSignup()}
          />

          <MyButton
            icon={require('../../assets/image/gmail.png')}
            title='Login with Email'
            backgroundColor="#d1c72a"
            containerStyle={{ marginBottom: 30 }}
            titleStyle={{ color: "black" }}
            onPress={() => this._onEmailSignup()}

          />

        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  imgStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    resizeMode: 'cover'
  },

  mainViewStyle: {
    flex: 1,
    justifyContent: "flex-end"
  },
});
const mapStateToProps = (state) => {
  const common = state.indexReducer;
  const stateAkshita = state.indexAkshitaReducer;

  return {
    set_profile: stateAkshita.set_profile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // setVenderId: (user_id) => setVenderId(user_id),
      setProfilePic: (profile) => setProfilePic(profile),
      setName: (name) => setName(name),
      setFullName: (full_name) => setFullName(full_name),
      setAge: (age) => setAge(age),
      setWeight: (weight) => setWeight(weight),
      setHeight: (height) => setHeight(height),
    },
    dispatch,
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);
