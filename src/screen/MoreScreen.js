import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    Image,
    TouchableOpacity,
    Dimensions,
    ToastAndroid,

} from 'react-native';
import { getWidth } from '../common/Layout';
import Font from '../common/Font';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import BottomNav from '../components/BottomNav';
import Header from '../components/Header';
import MoreOption from '../components/MoreOption';
import Prefs, { setPrefs } from '../common/Prefs';
import { setIsPrivacyPolicy, setIsStore, setIsTermsAndCondition } from '../redux_store/actions/indexActionsAkshita';
import { GoogleSignin } from '@react-native-community/google-signin';
import WebsiteScreen from './WebsiteScreen';



class MoreScreen extends Component {
    constructor(props) {
        super(props);
    }

    _onPrivacyPolicy = () => {
        this.props.navigation.navigate('WebsiteScreen', { url: 'https://boxprotraining.co.uk/index.php/privacy-policy/' })
    }

    _onTermsAndCondition = () => {
        this.props.navigation.navigate('WebsiteScreen', { url: 'https://boxprotraining.co.uk/index.php/terms-conditions/' })
    }

    _onShopStore = () => {
        this.props.navigation.navigate('WebsiteScreen', { url: 'https://boxprotraining.co.uk/index.php/shop/' })
    }
    _onLogout = async () => {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            ToastAndroid.show("Signed out", ToastAndroid.LONG, ToastAndroid.CENTER)
            console.log('signed out');
            setPrefs(Prefs.IS_REGISTER, "");
            this.props.navigation.navigate('SignUpScreen')
        } catch (error) {
            console.error(error);
        }
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.mainViewStyle}>
                    <Header
                        bgColor={Colors.whiteText}
                        barStyle="dark-content"
                        title="More"
                    />

                    <View style={{ flex: 1 }}>

                        <View style={{ height: 45, backgroundColor: Colors.lightgray, justifyContent: 'center', alignItems: 'baseline' }} >
                            <Text style={
                                styles.textHeaderStyle
                            }>
                                {'Support'}
                            </Text>
                        </View>
                        <MoreOption
                            // onPress={ }
                            image={require('../../assets/image/workout_.png')}
                            title="Sample Workout"
                        />
                        <MoreOption
                            // onPress={ }
                            image={require('../../assets/image/bug.png')}
                            title="Report a Bug"
                        />
                        <MoreOption
                            onPress={() => this._onPrivacyPolicy()}
                            // onPress={() => this.props.setIsPrivacyPolicy(!this.props.isPrivacyPolicy)}
                            image={require('../../assets/image/workout_.png')}
                            title="Privacy Policy"
                        />
                        <MoreOption
                            onPress={() => this._onTermsAndCondition()}
                            // onPress={() => this.props.setIsTermsAndCondition(!this.props.isTermsAndCondition)}
                            image={require('../../assets/image/terms_conditions.png')}
                            title="Terms and Conditions"

                        />
                        <MoreOption
                            onPress={() => this._onShopStore()}
                            // onPress={() => this.props.setIsStore(!this.props.isStore)}
                            image={require('../../assets/image/store.png')}
                            title="Online Store"
                        />


                        <View style={{ height: 1, marginVertical: 15, backgroundColor: 'lightgray', justifyContent: 'center', alignItems: 'baseline' }} >
                            <Text style={styles.textHeaderStyle}>{''} </Text>
                        </View>

                        <MoreOption
                            onPress={() => this._onLogout()}
                            image={require('../../assets/image/logout.png')}
                            title="Log Out"
                        />
                    </View>

                    <BottomNav
                        navigation={this.props.navigation}
                        bgColor={'#bdcf24'}
                        isMore={true}
                    />


                </View>
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        width, height,
        flex: 1,
        // justifyContent: 'center',
        backgroundColor: 'white',
    },
    textHeaderStyle: {
        marginLeft: 10,
        fontSize: 16,
        fontWeight: '600',
        fontFamily: Font.Roboto,
        textAlign: 'center',
        color: "gray",
    },
    mainViewStyle: {
        flex: 1
    },
});
const mapStateToProps = (state) => {
    const ss = state.indexReducer;
    const stateAkshita = state.indexAkshitaReducer;

    return {
        // vendor_id: ss.vendor_id,
        isStore: stateAkshita.isStore,
        isPrivacyPolicy: stateAkshita.isPrivacyPolicy,
        isTermsAndCondition: stateAkshita.isTermsAndCondition

    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            // setVenderId: (user_id) => setVenderId(user_id),
            setIsStore: (isStore) => setIsStore(isStore),
            setIsPrivacyPolicy: (isPrivacyPolicy) => setIsPrivacyPolicy(isPrivacyPolicy),
            setIsTermsAndCondition: (isTermsAndCondition) => setIsTermsAndCondition(isTermsAndCondition)
        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(MoreScreen);
