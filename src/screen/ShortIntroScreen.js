import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    Image,
    TouchableOpacity,
    Dimensions,
    ImageBackground,
    StatusBar,
    ScrollView,
} from 'react-native';
import { getWidth } from '../common/Layout';
import Font from '../common/Font';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MyButton from '../components/MyButton';
import Colors from '../common/Colors';
import SelectedVideoPlayer from '../components/SelectedVideoPlayer';
import PlayingVideoScreen from './PlayingVideoScreen';
import { setVideoUrl } from '../redux_store/actions/indexActionsJaswant';
import Header from '../components/Header';
import { StackActions } from '@react-navigation/native';


class ShortIntroScreen extends Component {
    constructor(props) {
        super(props);
    }

    onClickTry = () => {
        this.props.setVideoUrl(this.props.video_url)
        this.props.navigation.navigate('PlayingVideoScreen');
    }

    _onMayBeLater = () => {
        this.props.navigation.dispatch(
            StackActions.replace('HomeScreen'),
        )
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>

                <View style={styles.mainViewStyle}>
                    {/* <Header
                        bgColor='#22211d'
                        barStyle="light-content"
                        // isCross={true}
                    /> */}
                    <StatusBar
                        backgroundColor={Colors.themeColor}
                        barStyle='light-content'
                    />
                    <ScrollView
                        contentContainerStyle={{ flex: 1, alignItems: "center" }}
                    >

                        <Image
                            source={require('../../assets/image/logo_g.png')}
                            style={styles.imgStyle}
                        />

                        <TouchableOpacity
                            onPress={() => this.onClickTry()}
                            style={{
                                width: Dimensions.get('window').width - 20,
                                height: 200,
                                marginVertical: 10,
                                borderRadius: 25,
                            }}
                        >

                            <ImageBackground
                                source={require('../../assets/image/gym.png')}
                                imageStyle={{
                                    borderRadius: 20,
                                    height: 190,
                                }}
                                style={{
                                    flex: 1,
                                    justifyContent: "center",
                                    alignItems: 'center',
                                }}
                            >
                                <Image
                                    source={require('../../assets/image/play.png')}
                                    style={{ height: 50, width: 50 }}
                                />
                            </ImageBackground>
                        </TouchableOpacity>


                        <Text style={[styles.textStyle, { fontSize: 18 }]}>
                            {'Try A Short Intro Workout'}
                        </Text>

                        <Text style={[styles.textStyle, { fontWeight: '400', textAlign: "center" }]}>
                            {'Learn the ins and outs from the Pros and see how they bring the class to you with their unique mix of punch combos and style'}
                        </Text>




                        <TouchableOpacity
                            onPress={() => { this.onClickTry() }}
                            style={styles.tryIt}
                        >
                            <Text style={{ fontSize: 20, fontFamily: Font.SourceSansPro }}>  {'TRY IT'}   </Text>
                        </TouchableOpacity>


                        <TouchableOpacity
                            onPress={() => this._onMayBeLater()}
                        >
                            <Text style={[styles.textStyle, { textAlign: 'center' }]}>{'May be Later'} </Text>

                        </TouchableOpacity>

                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: Colors.themeColor,
    },
    imgStyle: {
        width: 100,
        height: 100,
        borderRadius: 100,
        marginVertical: 30
    },
    textStyle: {
        fontSize: 16,
        fontWeight: 'normal',
        marginVertical: 20,
        fontFamily: Font.SourceSansPro,
        color: Colors.whiteText,
    },
    mainViewStyle: {
        // marginHorizontal: 80,
        // alignItems: 'center',
        flex: 1,
    },
    tryIt: {
        width: getWidth(340),
        backgroundColor: '#d2c500',
        justifyContent: "center",
        alignItems: 'center',
        borderRadius: 5,
        paddingVertical: 14
    },
});

const mapStateToProps = (state) => {
    const ss = state.indexReducer;
    const Jaswant = state.indexJasReducer;


    return {
        // vendor_id: ss.vendor_id,
        volume_is: Jaswant.volume_is,
        video_url: Jaswant.video_url,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            // setVenderId: (user_id) => setVenderId(user_id),
            setVideoUrl: (video_url) => setVideoUrl(video_url)

        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(ShortIntroScreen);
