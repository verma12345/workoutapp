import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  Dimensions,
  FlatList,
  ScrollView,
} from 'react-native';
import { getWidth } from '../common/Layout';
import Font from '../common/Font';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ViewPagerScreen from './ViewPagerScreen';
import { MyPager } from '../components/MyPager';
import { hitVimeoAPI, setActiveDot, setSeeAll, setVideoUrl } from '../redux_store/actions/indexActionsJaswant';
import Colors from '../common/Colors';
import SelectedVideoPlayer from '../components/SelectedVideoPlayer';
import { BoxproFreePremium } from '../components/BoxproFreePremium';
import ProfileScreen from './ProfileScreen';
import BottomNav from '../components/BottomNav';
import VideoTitle from '../components/VideoTitle';
import Header from '../components/Header';

class HomeScreen extends Component {
  constructor(props) {
    super(props);
  }

  _navigation = (url) => {
    console.log(url.sources);
    this.props.setVideoUrl(url.sources)
    this.props.navigation.navigate('PlayingVideoScreen');
  }


  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.mainViewStyle}>
          {/* <Header
            // bgColor='#22211d'
            bgColor={Colors.themeColor}
            barStyle="light-content"
            title='Home'
          /> */}

          <ScrollView>

            <Text style={styles.txtStyle} >{'SAMPLE WORKOUT'} </Text>
            <MyPager
              onPageSelected={(index) => { this.props.setActiveDot(index.nativeEvent.position) }}
              list={this.props.video_list}
              navigation={this._navigation}
            />

            <ViewPagerScreen
              list={this.props.video_list}
            />

            <VideoTitle type='FREE'
              onSeeAll={() => {
                // alert('comming soon...')
                this.props.setSeeAll(!this.props.seeAll1)
              }}
            />
            <BoxproFreePremium

              //  data={this.state.data.slice(this.state.section*15,(this.state.section+1)*15)}
              // list={this.props.video_list}
              list={this.props.seeAll1 ? this.props.video_list : this.props.video_list.slice(0, 2)}
              navigation={this._navigation}

            />

            <VideoTitle type='PREMIUM' />
            <BoxproFreePremium
              list={this.props.video_list}
              navigation={this._navigation}
            />



          </ScrollView>
          <BottomNav
            navigation={this.props.navigation}
            bgColor={'#bdcf24'}
            isHome={true}
          />
        </View>
      </SafeAreaView>
    );
  }
}
const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  mainViewStyle: {
    backgroundColor: Colors.themeColor,
    flex: 1
  },
  txtStyle: {
    fontSize: 20,
    // fontFamily: Font.Roboto,
    fontWeight: '600',
    margin: 20,
    color: Colors.whiteText
  }
});


const mapStateToProps = (state) => {
  const ss = state.indexReducer;
  const jas = state.indexJasReducer;
  return {
    video_list: jas.video_list,
    volume_is: jas.volume_is,
    seeAll1: jas.seeAll1,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      setActiveDot: (position) => setActiveDot(position),
      setVideoUrl: (url) => setVideoUrl(url),
      setSeeAll: (isTrue) => setSeeAll(isTrue),
      hitVimeoAPI: (param) => hitVimeoAPI(param),
    },
    dispatch,
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
