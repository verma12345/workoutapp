import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    Image,
    TouchableOpacity,
    Dimensions,
    FlatList,
    ToastAndroid,
    Alert,
    StatusBar
} from 'react-native';
import { getWidth } from '../common/Layout';
import Font from '../common/Font';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import BottomNav from '../components/BottomNav';
import Header from '../components/Header';
import ProfileSection from '../components/ProfileSection';
import AchivementOption from '../components/AchivementOption';
import ImagePicker from 'react-native-image-crop-picker';
import { setIndex, setProfilePic } from '../redux_store/actions/indexActionsAkshita';
import { setAge, setFullName, setHeight, setName, setWeight } from '../redux_store/actions/indexActionsJaswant';




class ProfileScreen extends Component {
    constructor(props) {
        super(props);
    }

    // _onEdit = () => {
    //     ToastAndroid.show('Comming soon..', ToastAndroid.SHORT)
    // }
    _index = (index) => {
        this.props.setIndex(index)
    }

    renderItem = (dataItem) => {
        return (

            <AchivementOption
                data_item={dataItem.item}
                index={dataItem.index}
                index1={this.props.index}
                setIndex={this._index}

            />
        )
    }


    openPickerGallery = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            isCamera: true,
        }).then((image) => {
            this.props.setProfilePic(image.path);
        });
    };

    openPickerCamera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
        }).then((image) => {
            this.props.setProfilePic(image).path;
        });
    };

    showAlert = () => {
        Alert.alert(
            'Upload logo',
            'Please choose Camera or Gallery',
            [
                {
                    text: 'Camera',
                    onPress: () => {
                        this.openPickerCamera();
                    },
                },
                {
                    text: 'Gallery',
                    onPress: () => {
                        this.openPickerGallery();
                    },
                },
            ],
            {
                cancelable: false,
            },
        );
    }

    _listHeaderComponent = () => {
        return (
            <View style={{
                marginHorizontal: 15,
                justifyContent: 'space-between',
                alignItems: 'center',
                marginVertical: 5,
                flexDirection: 'row',
                marginTop: 90,
            }}>
                <Text style={styles.textStyle2}>{'Recent Achievments'} </Text>
                {/* <Text style={styles.textStyle1}>{'See All'} </Text> */}
            </View>

        )
    }


    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.mainViewStyle}>


                    <View style={{ flex: 0.3, backgroundColor: "#bdcf24", alignItems: "center" }} >
                        <StatusBar backgroundColor='#bdcf24' barStyle='light-content' />
                        <Text style={{ fontSize: 20, color: Colors.textColor, fontFamily: Font.Roboto }}>Profile</Text>
                    </View>

                    <ProfileSection
                        // onEdit={this._onEdit}
                        onPress={() => this.showAlert()}
                        set_profile={this.props.set_profile}
                        profile={this._profile}
                        name={this.props.name}
                        full_name={this.props.full_name}
                        age={this.props.name}
                        weight={this.props.weight}
                        height={this.props.height}
                    />

                    <View style={{ height: 5, backgroundColor: Colors.headerColor }} />

                    <View style={{ flex: 1 }}>




                        <View style={{ flex: 1, justifyContent: "space-around", alignItems: "center" }}>
                            <FlatList
                                data={this.props.achievements_list}
                                renderItem={this.renderItem}
                                numColumns={2}
                                keyExtractor={(item, index) => 'key' + index}
                                scrollEnabled={true}
                                showsVerticalScrollIndicator={false}
                                ListHeaderComponent={this._listHeaderComponent}
                            />
                        </View>
                    </View>


                    <BottomNav
                        navigation={this.props.navigation}
                        bgColor='#bdcf24'
                        isProfile={true}
                    />

                </View>
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        width, height,
        flex: 1,
        // justifyContent: 'center',
        backgroundColor: 'white',
    },
    imgStyle: {
        width: getWidth(130),
        height: getWidth(130),
        alignSelf: 'center'
    },
    textStyle2: {
        fontSize: 18,
        fontWeight: '900',
        fontFamily: Font.Roboto,
        color: Colors.grayColor,
    },
    textStyle1: {
        fontSize: 14,
        fontWeight: '400',
        fontFamily: Font.Roboto,
        color: Colors.grayColor,
        textDecorationLine: 'underline'
    },
    mainViewStyle: {
        flex: 1
    },
});
const mapStateToProps = (state) => {
    const ss = state.indexReducer;
    const stateAkshita = state.indexAkshitaReducer;
    const jaswant = state.indexJasReducer;

    return {
        // vendor_id: ss.vendor_id,
        achievements_list: stateAkshita.achievements_list,
        index: stateAkshita.index,
        set_profile: stateAkshita.set_profile,

        name: jaswant.name,
        full_name: jaswant.full_name,
        age: jaswant.age,
        weight: jaswant.weight,
        height: jaswant.height,

    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            // setVenderId: (user_id) => setVenderId(user_id),
            setProfilePic: (profile) => setProfilePic(profile),
            setIndex: (index) => setIndex(index),
            setName: (name) => setName(name),
            setFullName: (full_name) => setFullName(full_name),
            setAge: (age) => setAge(age),
            setWeight: (weight) => setWeight(weight),
            setHeight: (height) => setHeight(height),
        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
