import { TouchableOpacity, Text, View, Image, StyleSheet } from 'react-native';
import React from 'react';
import Font from '../common/Font';
import Colors from '../common/Colors';

const VideoTitle = (props) => {
  return (
    <View style={[styles.mainStyle, props.containerStyle]}>
      <Text style={styles.txtStyle} > {"BOXPRO"} <Text style={[styles.txtStyle, { color: 'yellow' }]} > {props.type} </Text> {'WORKOUT'} </Text>
      <TouchableOpacity
        onPress={props.onSeeAll}
      >
        <Text style={styles.txtStyle} >{'SEE ALL'} </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({

  myshadow: {
    shadowColor: "#1050e6",
    shadowOpacity: 0.15,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    // elevation: 2,
  },
  mainStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 10

  },
  txtStyle: {
    fontSize: 18,
    // fontFamily: Font.SourceSansPro,
    color: Colors.whiteText
  }

});
export default VideoTitle;