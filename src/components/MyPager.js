import React, { useState, useRef } from 'react';
import { StyleSheet, View, Platform, Text, TouchableOpacity, Image, Dimensions, ImageBackground } from 'react-native';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import Video from 'react-native-video';
import ViewPager from '@react-native-community/viewpager';
import { getWidth } from '../common/Layout';
import Colors from '../common/Colors';

export const MyPager = (props) => {

    const videoPlayer = useRef(null);
    const [duration, setDuration] = useState(0);
    const [paused, setPaused] = useState(true);

    const [currentTime, setCurrentTime] = useState(0);
    const [playerState, setPlayerState] = useState(PLAYER_STATES.PAUSED);
    const [isLoading, setIsLoading] = useState(true);

    const onSeek = (seek) => {
        videoPlayer?.current.seek(seek);
    };

    const onSeeking = (currentVideoTime) => setCurrentTime(currentVideoTime);

    const onPaused = (newState) => {

        setPaused(!paused);
        setPlayerState(newState);
    };

    const onReplay = () => {
        videoPlayer?.current.seek(0);
        setCurrentTime(0);
        if (Platform.OS === 'android') {
            setPlayerState(PLAYER_STATES.PAUSED);
            setPaused(true);
        } else {
            setPlayerState(PLAYER_STATES.PLAYING);
            setPaused(false);
        }
    };

    const onProgress = (data) => {
        if (!isLoading) {
            setCurrentTime(data.currentTime);
        }
    };

    const onLoad = (data) => {
        setDuration(Math.round(data.duration));
        setIsLoading(false);
    };

    const onLoadStart = () => setIsLoading(true);

    const onEnd = () => {
        setPlayerState(PLAYER_STATES.ENDED);
        setCurrentTime(duration);
    };


    let row = props.list.map((item, index) => {
        return (
            <View
                key={index}
                style={[{
                    flex: 1,
                    // elevation: 10,
                    borderRadius: 10,
                    // backgroundColor:"red",
                    margin: 5,
                    justifyContent: "center",
                }, styles.myshadow]}
            >


                    <TouchableOpacity
                        onPress={() => props.navigation(item)}
                        style={{
                            flex: 1,
                            backgroundColor: 'rgb(42, 56, 65)',
                            borderRadius: 10,
                        }}
                    >
                        <ImageBackground
                            source={item.thumb}
                            imageStyle={{
                                borderRadius: 10,
    
                            }}
                            style={{
                                // height: 220,
                                flex: 1,
                                justifyContent: "center",
                                alignItems: 'center',
                                margin: 3
                            }}
                        >
                            <Image
                                source={require('../../assets/image/play.png')}
                                style={{ height: 50, width: 50 }}
                            />
                        </ImageBackground>
                    </TouchableOpacity>

            </View>

        )
    })

    return (

        < View style={{ marginLeft: 10 }} >
            <ViewPager
                // pageMargin={10}
                onPageSelected={props.onPageSelected}
                style={styles.viewPager} initialPage={0}>

                {row}

            </ViewPager>
        </View >
    );
};

const styles = StyleSheet.create({
    viewPager: {
        height: 250,
        // margin: 10,

    },
    txtStyle: {
        fontSize: 16,
        color: Colors.whiteColor
    },
    pagerStyle: {
        flex: 1,
        borderRadius: 10,
        elevation: 5,
        backgroundColor: 'cyan',
        alignItems: "center",
        justifyContent: 'center'
    },
    myshadow: {
        shadowColor: "#1050e6",
        shadowOpacity: 0.15,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowRadius: 8.30,
        elevation: 13,
    },
});