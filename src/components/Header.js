import { TouchableOpacity, Text, View, StyleSheet, Image, ToastAndroid, StatusBar, Dimensions } from 'react-native';
import React from 'react';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';
import Font from '../common/Font';

const Header = (props) => {
  return (
    <View style={{
      height: 40,
      elevation: 2,
      backgroundColor: props.bgColor,
      flexDirection: 'row',
      alignItems: "center",
      width: Dimensions.get('window').width,
      justifyContent: "center"
    }}>
      <StatusBar backgroundColor={props.bgColor}
        barStyle={props.barStyle}
      />

      {props.isCross ? <TouchableOpacity
        onPress={() => props.navigation.goBack()}
        style={{ width: 40, height: 40, justifyContent: "center", position: "absolute", left: 5, alignItems: "center", alignSelf: 'baseline' }}
      >
        <Image
          source={require("../../assets/image/back.png")}
          style={{ width: 30, height: 15, tintColor: Colors.themeColor }}
        />
      </TouchableOpacity> : null}

      <Text style={styles.textHeaderStyle}>
        {props.title}
      </Text>


    </View>
  );
};

const styles = StyleSheet.create({

  myshadow: {
    shadowColor: "#1050e6",
    shadowOpacity: 0.15,

    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    elevation: 13,
  },
  textHeaderStyle: {
    fontSize: 18,
    fontWeight: '600',
    fontFamily: Font.Roboto,
    textAlign: 'center',
    color: Colors.themeColor,
    alignSelf: "center"
  },
});
export default Header;
