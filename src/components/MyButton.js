import { TouchableOpacity, Text, View, Image, StyleSheet } from 'react-native';
import React from 'react';
import Font from '../common/Font';

const MyButton = (props) => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={[{
        backgroundColor: props.backgroundColor,
        paddingVertical: 18,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: "center",
        marginHorizontal: 30,
        borderRadius: 5,
        marginVertical: 10,
      }, props.containerStyle]}
    >
      <Image
        style={{ width: 25, height: 25 }}
        source={props.icon} />
      <Text style={[{
        color: 'white',
        marginLeft: 20,
        fontFamily: Font.Roboto,
        fontSize: 17
      }, props.titleStyle]} >
        {props.title}
      </Text>
    </TouchableOpacity>

  );
};
const styles = StyleSheet.create({

  myshadow: {
    shadowColor: "#1050e6",
    shadowOpacity: 0.15,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    // elevation: 2,
  },


});
export default MyButton;