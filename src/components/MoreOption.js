import React from 'react'
import { Dimensions, Image, StyleSheet, Text, ToastAndroid, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import Font from '../common/Font';
import { getWidth } from '../common/Layout';
import WebView from "react-native-webview";

const MoreOption = (props) => {
    return (

        <TouchableOpacity
            onPress={props.onPress}
            style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingVertical: 15
            }}>

            <Image
                source={props.image}
                style={{
                    width: 30,
                    height: 30,
                    marginLeft: 15
                }}
            />
            <Text style={{
                fontSize: 14,
                fontWeight: '600',
                fontFamily: Font.Roboto,
                textAlign: 'center',
                color: "gray",
                marginLeft: 15
            }} >{props.title}
            </Text>

        </TouchableOpacity>
    )
}

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({

    container: {
        width, height,
        flex: 1,
        backgroundColor: 'white',
    },
    imgStyle: {
        width: getWidth(120),
        height: getWidth(120),
    },
    mainViewStyle: {
        flex: 1
    },
});

export default MoreOption;