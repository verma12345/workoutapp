import React from 'react'
import { Alert, Dimensions, Image, ImageBackground, StyleSheet, Text, ToastAndroid, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import Font from '../common/Font';

const ProfileSection = (props) => {

    return (
        <View style={{
            top: 40,
            left: Dimensions.get('window').width / 9 + 2,
            alignItems: "center",
            justifyContent: 'center',
            backgroundColor: "white",
            borderRadius: 10,
            position: 'absolute',
            elevation: 10,
            width: Dimensions.get('window').width / 1.3,
            height: 200
        }} >

            <ImageBackground
                source={
                    props.set_profile ?
                        { uri: props.set_profile }
                        : require('../../assets/image/avatar.png')
                }
                style={{ height: 80, width: 80, }}
                imageStyle={{ borderRadius: 100 }}
                onPress={props.onPress}>

                <TouchableOpacity
                    onPress={props.onPress}
                    style={{ right: 5, bottom: 5, position: 'absolute', }}
                >
                    <Image
                        style={{ height: 20, width: 20, tintColor: Colors.textColor, }}
                        source={require('../../assets/image/camera.png')}
                    />
                </TouchableOpacity>
            </ImageBackground>

            <View style={{ flexDirection: 'row', justifyContent: 'center', marginVertical: 10 }}>
                <View style={{ flex: 1, alignItems: "flex-end" }}>
                    <Text style={styles.textStyle1}> {'Profile Name:'} </Text>
                    <Text style={styles.textStyle1}> {'Full Name:'} </Text>
                    <Text style={styles.textStyle1}> {'Age:'} </Text>
                    <Text style={styles.textStyle1}> {'Weight:'} </Text>
                    <Text style={styles.textStyle1}> {'Height:'} </Text>
                </View>
                <View style={{ flex: 1 }}>
                    <Text style={styles.textStyle2}> {props.name} </Text>
                    <Text style={styles.textStyle2}> {props.full_name} </Text>
                    <Text style={styles.textStyle2}> {'22'} </Text>
                    <Text style={styles.textStyle2}> {'14 Stone 4 Pound'} </Text>
                    <Text style={styles.textStyle2}> {`6' 4"`} </Text>
                </View>

            </View>
        </View>
    )
}

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        width, height,
        flex: 1,
        backgroundColor: 'white',
    },
    imgStyle: {
        width: 100,
        height: 100,
    },
    textStyle2: {
        fontSize: 12,
        fontFamily: Font.Roboto,
        color: 'gray',

    },
    textStyle1: {
        fontSize: 12,
        fontFamily: Font.Roboto,
        color: Colors.lightgray,
        textAlign: 'center'
    },
    mainViewStyle: {
        flex: 1
    },
});

export default ProfileSection;