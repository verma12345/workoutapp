import React, { useEffect } from 'react'
import { StyleSheet, Text, ToastAndroid, View } from 'react-native'

import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
} from '@react-native-community/google-signin';
import { TouchableOpacity } from 'react-native-gesture-handler';


const GoogleSignUp = (props) => {

    useEffect(() => {
        GoogleSignin.configure({
            scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
            webClientId: '698858406842-7s4qfg5crok9dp2p5ircpqv0no2v9et9.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
            offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
            // hostedDomain: '', // specifies a hosted domain restriction
            // loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
            forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
            // accountName: '', // [Android] specifies an account name on the device that should be used
            // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
        });
    }, [])

    const signIn = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            //   this.setState({ userInfo });
            console.log({ userInfo });
            alert(
                +"\n" + userInfo.user.givenName
                + "\n" + userInfo.user.familyName 
                + "\n" + userInfo.user.email
                +"\n" + userInfo.user.name
                +"\n" + userInfo.user.photo

                )
        } catch (error) {
            console.log({ error });
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
            } else {
                // some other error happened
            }
        }
    };

    const signOut = async () => {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            ToastAndroid.show("Signed out", ToastAndroid.SHORT, ToastAndroid.CENTER)
            console.log('signed out');
            //   this.setState({ user: null }); // Remember to remove the user from your app's state as well
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: 'center' }} >
            <GoogleSigninButton
                style={{ width: 192, height: 48 }}
                size={GoogleSigninButton.Size.Wide}
                color={GoogleSigninButton.Color.Dark}
                onPress={signIn}
            // disabled={this.state.isSigninInProgress} 
            />

            <TouchableOpacity
                style={styles.logOutStyle}
                onPress={signOut}
            >
                <Text style={styles.txt} >
                    {"Logout"}
                </Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    logOutStyle: {
        margin: 20,
        backgroundColor: "red",
        padding: 5,
        paddingHorizontal: 20,
        borderRadius: 3
    },
    txt: {
        color: "white"
    }
})
export default GoogleSignUp