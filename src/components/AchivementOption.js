import React from 'react'
import { Dimensions, Image, StyleSheet, Text, ToastAndroid, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import Font from '../common/Font';
import { getWidth } from '../common/Layout';

const AchivementOption = (props) => {
    return (
        <TouchableOpacity
            onPress={() => props.setIndex(props.index)}
            style={{
                justifyContent: 'center',
                width: Dimensions.get('window').width / 2.5,
                height: Dimensions.get('window').width / 4,
                alignItems: 'center',
                padding: 15,
                margin: 10,
                backgroundColor: "white",
                elevation: 10,
                borderRadius: 5,
            }}>
            <Image
                source={props.data_item.image}

                style={{
                    width: Dimensions.get('window').width / 9,
                    height: Dimensions.get('window').width / 9,
                    tintColor:props.index == props.index1? '#bdcf24' : 'gray'
                }}
            />
            <Text style={{
                fontSize: 14,
                fontWeight: '600',
                fontFamily: Font.Roboto,
                textAlign: 'center',
                color: "gray"
            }} >{props.data_item.title} </Text>

        </TouchableOpacity>
    )
}

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        width, height,
        flex: 1,
        // justifyContent: 'center',
        backgroundColor: 'white',
    },
    imgStyle: {
        width: getWidth(120),
        height: getWidth(120),
        // alignSelf: 'center'
    },
    textStyle2: {
        fontSize: 18,
        fontWeight: '900',
        fontFamily: Font.Roboto,
        color: Colors.textDarkColor,
    },
    textStyle1: {
        fontSize: 13,
        fontWeight: '400',
        fontFamily: Font.Roboto,
        color: Colors.grayColor,
        textDecorationLine: 'underline',
    },
    mainViewStyle: {
        flex: 1
    },
});

export default AchivementOption;