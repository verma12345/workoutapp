import React from 'react';
import { StyleSheet, View, TouchableOpacity, Image, Dimensions, ImageBackground, FlatList } from 'react-native';
import Colors from '../common/Colors';

export const BoxproFreePremium = (props) => {

    const _renderItem = (dataItem) => {
        return (
            <View
                key={dataItem.index}
                style={{
                    marginBottom: 20,
                    flex: 1,
                    marginLeft: 10,
                    marginRight: 10,
                    elevation:10
                }}
            >

                <TouchableOpacity
                    onPress={() => props.navigation(dataItem.item)}
                    style={{
                        padding:3,
                        backgroundColor:'rgb(42, 56, 65)',
                        borderRadius:10,
                    }}
                >
                    <ImageBackground
                        source={dataItem.item.thumb}
                        imageStyle={{
                            borderRadius: 7,
                        }}
                        style={{
                            flex: 1,
                            height:100,
                            justifyContent: "center",
                            alignItems: 'center',
                        }}
                    >
                        <Image
                            source={require('../../assets/image/play.png')}
                            style={{ height: 30, width: 30 }}
                        />
                    </ImageBackground>
                </TouchableOpacity>

            </View>
        )
    }

    return (
        < View style={{ marginTop: 10, marginHorizontal: 10 }} >

            <FlatList
                data={props.list}
                renderItem={_renderItem}
                // horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) => "key" + index}
                numColumns={2}
            />

        </View >
    );
};
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    viewPager: {
        height: 250,
        // margin: 10,

    },
    txtStyle: {
        fontSize: 16,
        color: Colors.whiteColor
    },
    pagerStyle: {
        flex: 1,
        borderRadius: 10,
        elevation: 5,
        backgroundColor: 'cyan',
        alignItems: "center",
        justifyContent: 'center'
    }
});