import { TouchableOpacity, Text, View, StyleSheet, Image, ToastAndroid, StatusBar, Dimensions } from 'react-native';
import React from 'react';
import Colors from '../common/Colors';
import Font from '../common/Font';

const EndVideo = (props) => {

  const _onClick = () => {
    alert('Comming Soon...')
  }

  return (

    <TouchableOpacity style={{
      flexDirection: "row",
      padding: 5,
      borderBottomLeftRadius: 10,
      backgroundColor: "#0003",

    }} >


      <TouchableOpacity
        onPress={() => { _onClick() }}
      >
        <Image
          source={require("../../assets/image/info.png")}
          style={{ height: 40, width: 40, margin: 10 }}
        />
      </TouchableOpacity>


      <TouchableOpacity
        style={{ borderRadius: 100,paddingLeft:5, backgroundColor: "green", flexDirection: 'row', 
        justifyContent: "center", height: 40, margin: 10, alignItems: "center" }}
        onPress={() => { _onClick() }}
      >
        <Text style={{ color: 'white',fontSize:12,fontFamily:Font.Roboto }} > COMPLETED</Text>
        <View style={{ width: 30, height: 30,margin:5, backgroundColor: "white", borderRadius: 100 }} />
      </TouchableOpacity>



      <TouchableOpacity
        onPress={() => { _onClick() }}
      >
        <Image
          source={require("../../assets/image/like.png")}
          style={{ height: 40, width: 40, margin: 10 }}
        />
      </TouchableOpacity>




      <TouchableOpacity
        onPress={() => { _onClick() }}
      >
        <Image
          source={require("../../assets/image/dislike.png")}
          style={{ height: 40, width: 40, margin: 10 }}
        />
      </TouchableOpacity>




      <TouchableOpacity
        onPress={() => { _onClick() }}
      >
        <Image
          source={require("../../assets/image/share.png")}
          style={{ height: 30, width: 30, margin: 10, resizeMode: 'contain' }}
        />
      </TouchableOpacity>


    </TouchableOpacity>

  );
};

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({

  myshadow: {
    shadowColor: "#1050e6",
    shadowOpacity: 0.15,

    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    elevation: 13,
  },
  textHeaderStyle: {
    fontSize: 16,
    fontWeight: '600',
    fontFamily: Font.Roboto,
    textAlign: 'center',
    color: Colors.grayColor,
  },
});
export default EndVideo;
