import React, { useState, useRef } from 'react';
import { StyleSheet, View, Platform, Text, TouchableOpacity, Image } from 'react-native';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import Video from 'react-native-video';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';
import EndVideo from './EndVideo';


const SelectedVideoPlayer = (props) => {

    const video = { uri: props.url }

    const videoPlayer = useRef(null);
    const [duration, setDuration] = useState(0);
    const [paused, setPaused] = useState(true);

    const [currentTime, setCurrentTime] = useState(0);
    const [playerState, setPlayerState] = useState(PLAYER_STATES.PAUSED);
    const [isLoading, setIsLoading] = useState(true);

    const onSeek = (seek) => {
        videoPlayer?.current.seek(seek);
    };

    const onSeeking = (currentVideoTime) => setCurrentTime(currentVideoTime);

    const onPaused = (newState) => {
        setPaused(!paused);
        setPlayerState(newState);
    };

    const onReplay = () => {
        props.setIsEndVideo(false)

        videoPlayer?.current.seek(0);
        setCurrentTime(0);
        if (Platform.OS === 'android') {
            setPlayerState(PLAYER_STATES.PAUSED);
            setPaused(true);
        } else {
            setPlayerState(PLAYER_STATES.PLAYING);
            setPaused(false);
        }
    };

    const onProgress = (data) => {
        if (!isLoading) {
            setCurrentTime(data.currentTime);
        }
    };

    const onLoad = (data) => {
        setDuration(Math.round(data.duration));
        setIsLoading(false);
    };

    const onLoadStart = () => setIsLoading(true);


    const onEnd = () => {
        setPlayerState(PLAYER_STATES.ENDED);
        setCurrentTime(duration);
        props.setIsEndVideo(true)
    };



    return (
        <View style={{
            width: "100%",
            height: "100%",
            marginHorizontal: 10,
            alignSelf: "center"
        }}>

            <Video
                onEnd={onEnd}
                onLoad={onLoad}
                onLoadStart={onLoadStart}
                posterResizeMode={'cover'}
                onProgress={onProgress}
                paused={paused}
                ref={(ref) => (videoPlayer.current = ref)}
                resizeMode={'contain'}
                source={{ uri: props.video_url }}
                volume={10}
                style={[styles.backgroundVideo, {
                    width: "100%",
                    height: "100%",
                    backgroundColor: Colors.themeColor
                }]}
            />

            <MediaControls
                isFullScreen={false}
                duration={duration}
                isLoading={isLoading}
                progress={currentTime}
                onPaused={onPaused}
                onReplay={onReplay}
                onSeek={onSeek}
                onSeeking={onSeeking}
                mainColor={Colors.themeColor}
                playerState={playerState}
                sliderStyle={{ containerStyle: { marginBottom: 10 }, thumbStyle: {}, trackStyle: {} }}
            />

        </View>

    );
};

const styles = StyleSheet.create({
    backgroundVideo: {

        paddingHorizontal: 15,

    },
    mediaControls: {
        height: getWidth(200),
        flex: 1,
        alignSelf: 'center',
    },
});

export default SelectedVideoPlayer;